package com.bsa.giphy.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GiphyDto {
    String name;
    List<String> gifs;

    public GiphyDto(String name, List<String> gifs) {
        this.name = name;
        this.gifs = gifs;
    }

}
