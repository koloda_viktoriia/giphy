package com.bsa.giphy.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestQueryDto {
    String query;
    Boolean force;

}
