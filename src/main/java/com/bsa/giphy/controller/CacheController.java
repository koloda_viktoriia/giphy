package com.bsa.giphy.controller;

import com.bsa.giphy.dto.GiphyDto;
import com.bsa.giphy.dto.RequestQueryDto;
import com.bsa.giphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("cache")
public final class CacheController {

    private final CacheService cacheService;

    @Autowired
    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @PostMapping("/generate")
    public GiphyDto generateGiphy(@RequestBody RequestQueryDto query) {
        var result = cacheService.generate(query.getQuery());
        if (result == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gif isn't found.");
        }
        return result;
    }

    @GetMapping
    public List<GiphyDto> get(@RequestParam(required = false) List<String> query) {
        var result = cacheService.getCache(query);
        if (result == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gif isn't found.");
        }
        return result;
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete() {
        cacheService.deleteCache();
    }

}
