package com.bsa.giphy.controller;

import com.bsa.giphy.dto.GiphyDto;
import com.bsa.giphy.dto.RequestQueryDto;
import com.bsa.giphy.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("user")
public final class UserController {
    private static Logger logger = Logger.getLogger("User Logger");
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    private Boolean validId(String id) {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");
        Matcher matcher = pattern.matcher(id);
        return (matcher.matches() && id.length() < 260);
    }

    @PostMapping("/{id}/generate")
    public String generate(@PathVariable("id") String id,
                           @RequestBody RequestQueryDto query) {
        System.out.println(id);
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        logger.info(id + " generates " + query + ".");
        System.out.println(query.getQuery());
        var result = userService.generateGif(id, query.getQuery(), query.getForce());
        if (result == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gif isn't found.");
        }
        return result;

    }

    @GetMapping("/{id}/all")
    public List<GiphyDto> getAll(@PathVariable("id") String id) {
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        logger.info(id + " gets all gifs.");
        return userService.getUserGifs(id);
    }

    @GetMapping("/{id}/history")
    public List<Map<String, String>> getHistory(@PathVariable("id") String id) {
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        logger.info(id + " gets own history.");
        return userService.getUserHistory(id);
    }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void cleanHislory(@PathVariable("id") String id) {
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        logger.info(id + " cleans own history.");
        userService.deleteUserHistory(id);

    }

    @GetMapping("/{id}/search")
    public String search(@PathVariable("id") String id, @RequestParam Map<String, String> query) {
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        Boolean force = query.containsKey("force");
        String isForce = force ? "with force." : "without force.";
        logger.info(id + " is looking for " + query.get("query") + isForce);
        var result = userService.getGif(id, query.get("query"), force);
        if (result == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User doesn't have this gif.");
        }
        return result;

    }

    @DeleteMapping("/{id}/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void reset(@PathVariable("id") String id, @RequestParam(required = false) List<String> query) {
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        logger.info(id + " resets own cache.");
        userService.deleteCache(id, query);
    }

    @DeleteMapping("/{id}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void clean(@PathVariable("id") String id) {
        if (!validId(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not valid id.");
        logger.info(id + " clean own data.");
        userService.deleteAllUserData(id);
    }
}
