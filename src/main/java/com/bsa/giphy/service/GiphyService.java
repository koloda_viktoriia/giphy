package com.bsa.giphy.service;

import com.bsa.giphy.repository.GiphyRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GiphyService {
    @Autowired
    private final GiphyRepository giphyRepository;
    @Value(value = "${giphy.api_key}")
    private String API_KEY;
    @Value(value = "${giphy.api_endpoint}")
    private String API_EDPOINT;

    public GiphyService(GiphyRepository giphyRepository) {
        this.giphyRepository = giphyRepository;
    }

    public String generateGif(String query) {
        HttpClient client = HttpClient.newHttpClient();
        try {
            var response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());

            ObjectMapper mapper = new ObjectMapper();
            final ObjectNode node = new ObjectMapper().readValue(response.body(), ObjectNode.class);
            ObjectNode[] gifs = mapper.readValue(node.get("data").toString(), ObjectNode[].class);

            List<String> idGifs = giphyRepository.getAllGifs().stream().
                    map(path -> path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf(".")))
                    .collect(Collectors.toList());
            String id = "";
            if (gifs == null) return null;
            for (var gif : gifs) {
                id = gif.get("id").toString().replaceAll("^\"|\"$", "");
                String finalId = id;
                if (idGifs.stream().noneMatch(image -> image.compareTo(finalId) == 0)) {
                    return id;
                }
            }
            return id;
        } catch (IOException | InterruptedException ex) {
            System.out.println("error");
            return null;
        }
    }

    private HttpRequest buildGetRequest(String query) {
        return HttpRequest
                .newBuilder()
                .uri(URI.create(
                        new StringBuilder(API_EDPOINT)
                                .append("?api_key=").append(API_KEY)
                                .append("&q=").append(query)
                                .append("&limit=25")
                                .toString()))
                .GET()
                .build();
    }
}
