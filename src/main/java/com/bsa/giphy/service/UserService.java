package com.bsa.giphy.service;

import com.bsa.giphy.dto.GiphyDto;
import com.bsa.giphy.repository.GiphyRepository;
import com.bsa.giphy.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final GiphyRepository giphyRepository;
    private final GiphyService giphyService;

    public UserService(UserRepository userRepository,
                       GiphyRepository giphyRepository,
                       GiphyService giphyService) {
        this.userRepository = userRepository;
        this.giphyRepository = giphyRepository;
        this.giphyService = giphyService;
    }

    public String generateGif(String id, String query, Boolean force) {

        if (force == null) {
            String path = copyGifToUser(id, query);
            if (path != null) return path;
        }
        giphyRepository.generateInCache(query, giphyService.generateGif(query));
        return copyGifToUser(id, query);
    }

    public String copyGifToUser(String id, String query) {
        GiphyDto userGif = giphyRepository.getByKeyInCache(query);
        if (userGif != null) {
            int randomIndex = 0 + (int) (Math.random() * userGif.getGifs().size());
            String gif = userGif.getGifs().get(randomIndex);
            return userRepository.copyGif(id, query, gif);
        }
        return null;
    }

    public List<GiphyDto> getUserGifs(String id) {
        return userRepository.getGifsById(id);
    }

    public List<Map<String, String>> getUserHistory(String id) {
        return userRepository.getHistoryById(id);
    }

    public void deleteUserHistory(String id) {
        userRepository.deleteHistoryById(id);
    }

    public String getGif(String id, String query, Boolean force) {
        if (!force) {
            String gif = userRepository.getGifFromCache(id, query);
            if (gif != null) return gif;
        }
        String gif = userRepository.getGifFromStorage(id, query);
        return gif;
    }

    public void deleteCache(String id, List<String> query) {
        userRepository.deleteUserCache(id, query);
    }

    public void deleteAllUserData(String id) {
        userRepository.deleteUserCache(id, null);
        userRepository.deleteUserStorage(id);
    }
}
