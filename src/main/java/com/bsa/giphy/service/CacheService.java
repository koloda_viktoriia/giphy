package com.bsa.giphy.service;

import com.bsa.giphy.dto.GiphyDto;
import com.bsa.giphy.repository.GiphyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public final class CacheService {
    private final GiphyRepository giphyRepository;
    private final GiphyService giphyService;

    @Autowired
    public CacheService(GiphyRepository giphyRepository, GiphyService giphyService) {
        this.giphyRepository = giphyRepository;
        this.giphyService = giphyService;
    }

    public GiphyDto generate(String query) {
        giphyRepository.generateInCache(query, giphyService.generateGif(query));
        return giphyRepository.getByKeyInCache(query);
    }


    public List<GiphyDto> getCache(List<String> query) {
        return giphyRepository.getListInCache(query);
    }

    public void deleteCache() {
        giphyRepository.deleteCacheData();
    }
}
