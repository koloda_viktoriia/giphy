package com.bsa.giphy.service;

import com.bsa.giphy.repository.GiphyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public final class GifService {
    private GiphyRepository giphyRepository;

    private GifService(GiphyRepository giphyRepository) {
        this.giphyRepository = giphyRepository;
    }

    public List<String> getGif() {
        return giphyRepository.getAllGifs();
    }
}
