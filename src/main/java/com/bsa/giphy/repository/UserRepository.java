package com.bsa.giphy.repository;


import com.bsa.giphy.dto.GiphyDto;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.*;

@Repository
public class UserRepository {
    private String USER_PATH = "bsa_giphy/users/";
    private Map<String, Map<String, List<String>>> usersCache = new LinkedHashMap<>();

    public String copyGif(String id, String query, String gif) {
        new File(USER_PATH + "/" + id).mkdir();
        new File(USER_PATH + "/" + id + "/" + query).mkdir();
        String pathToGif = USER_PATH + "/" + id + "/" + query + "/" + new File(gif).getName();
        Path userPath = Paths.get(pathToGif);
        Path cachePath = Path.of(gif);
        try {
            Files.copy(cachePath, userPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String path = new File(pathToGif).getAbsolutePath().replaceAll("\\\\", "/");
        updateCache(id, query, path);
        writeHistory(id, query, path);
        return '"' + path + '"';
    }

    private void updateCache(String id, String query, String gif) {
        List<String> gifList = new ArrayList<>();
        if (!usersCache.containsKey(id)) {
            gifList.add(gif);
            Map<String, List<String>> queryMap = new LinkedHashMap<>();
            queryMap.put(query, gifList);
            usersCache.put(id, queryMap);
        } else if (!usersCache.get(id).containsKey(query)) {
            gifList.add(gif);
            usersCache.get(id).put(query, gifList);
        } else {
            usersCache.get(id).get(query).add(gif);
        }
        System.out.println(usersCache);
    }

    private void writeHistory(String id, String query, String gif) {
        String path = USER_PATH + "/" + id + "/history.csv";
        LocalDate date = LocalDate.now();
        StringBuilder data = new StringBuilder(date.toString())
                .append(", ").append(query)
                .append(", ").append(gif);
        try {
            FileWriter fileWriter = new FileWriter(path, true);
            fileWriter.append(data.toString());
            fileWriter.append("\n");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<GiphyDto> getGifsById(String id) {
        List<GiphyDto> giphyCache = new ArrayList<>();
        String path = USER_PATH + "/" + id;
        if (Files.exists(Path.of(path))) {
            File[] dirs = new File(path).listFiles();
            for (File dir : dirs) {
                if (dir.isDirectory()) {
                    giphyCache.add(getByKeyInCache(id, dir.getName()));
                }
            }
            return giphyCache;
        }
        return null;
    }

    public GiphyDto getByKeyInCache(String id, String key) {
        String path = USER_PATH + "/" + id + "/" + key;
        if (Files.exists(Path.of(path))) {
            List<String> gifs = new ArrayList<>();
            File[] files = new File(path).listFiles();
            for (File file : files) {
                if (file.isFile()) {
                    gifs.add(file.getAbsolutePath().replaceAll("\\\\", "/"));
                }
            }
            return new GiphyDto(key, gifs);
        }
        return null;
    }

    public List<Map<String, String>> getHistoryById(String id) {
        String path = USER_PATH + "/" + id + "/history.csv";
        List<Map<String, String>> history = new ArrayList<>();
        File historyFile = new File(path);
        if (historyFile.isFile()) {
            try {
                BufferedReader bufferReader = new BufferedReader(new FileReader(path));
                String row = "";
                while ((row = bufferReader.readLine()) != null) {
                    List<String> data = List.of(row.split(","));
                    Map<String, String> record = new LinkedHashMap<>();
                    record.put("date", data.get(0).trim());
                    record.put("query", data.get(1).trim());
                    record.put("gif", data.get(2).trim());
                    history.add(record);
                }
                bufferReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return history;
        }
        return null;
    }

    public void deleteHistoryById(String id) {
        String path = USER_PATH + "/" + id + "/history.csv";
        File historyFile = new File(path);
        if (historyFile.isFile()) {
            try {
                Files.delete(Path.of(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getGifFromCache(String id, String query) {
        if (usersCache.containsKey(id) && usersCache.get(id).containsKey(query)) {
            List<String> gifs = usersCache.get(id).get(query);
            int randomIndex = (0 + (int) (Math.random() * gifs.size()));
            String gif = gifs.get(randomIndex);
            return '"' + gif + '"';
        }
        return null;
    }

    public String getGifFromStorage(String id, String query) {
        GiphyDto giphy = getByKeyInCache(id, query);

        if (giphy != null) {
            List<String> gifs = getByKeyInCache(id, query).getGifs();
            int randomIndex = (0 + (int) (Math.random() * gifs.size()));
            String gif = gifs.get(randomIndex);
            updateCache(id, query, gif);
            return '"' + gif + '"';
        }
        return null;

    }

    public void deleteUserCache(String id, List<String> query) {
        if (usersCache.containsKey(id)) {
            if (query == null) {
                usersCache.get(id).clear();
                System.out.println(usersCache);
                return;
            }
            for (String key : query) {
                usersCache.get(id).remove(key);
                System.out.println(usersCache);
            }
        }
    }

    public void deleteUserStorage(String id) {
        String userPath = USER_PATH + "/" + id;
        Path dir = Paths.get(userPath);
        try {
            Files.walk(dir)
                    .sorted(Comparator.reverseOrder())
                    .forEach(path -> {
                        try {
                            if (path.compareTo(Path.of(userPath)) != 0) {
                                Files.delete(path);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
