package com.bsa.giphy.repository;


import com.bsa.giphy.dto.GiphyDto;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Repository
public class GiphyRepository {
    private String CACHE_PATH = "bsa_giphy/cache";

    public GiphyRepository() {
        seed();
    }

    private void seed() {
        File giphyFolder = new File("bsa_giphy");
        if (!giphyFolder.exists()) {
            System.out.println("creating directory: " + giphyFolder.getName());
            giphyFolder.mkdir();
            new File(CACHE_PATH).mkdir();
            new File("bsa_giphy/users").mkdir();
        }
    }


    public void generateInCache(String query, String gifId) {
        try {
            InputStream in = new URL("https://i.giphy.com/media/" + gifId + "/giphy.webp").openStream();
            String gifPath = CACHE_PATH + "/" + query + "/" + gifId + ".gif";
            new File(CACHE_PATH + "/" + query).mkdir();
            Files.copy(in, Paths.get(gifPath), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GiphyDto getByKeyInCache(String key) {
        String path = CACHE_PATH + "/" + key;
        if (Files.exists(Path.of(path))) {
            List<String> gifs = new ArrayList<String>();
            File[] files = new File(path).listFiles();
            for (File file : files) {
                if (file.isFile()) {
                    gifs.add(file.getAbsolutePath().replaceAll("\\\\", "/"));
                }
            }
            return new GiphyDto(key, gifs);
        }
        return null;
    }


    public List<GiphyDto> getListInCache(List<String> query) {
        List<GiphyDto> giphyCache = new ArrayList<>();
        if (query == null) {
            File[] dirs = new File(CACHE_PATH).listFiles();
            for (File dir : dirs) {
                if (dir.isDirectory()) {
                    giphyCache.add(getByKeyInCache(dir.getName()));
                }
            }
            return giphyCache;
        }
        for (var key : query) {
            giphyCache.add(getByKeyInCache(key));
        }
        return giphyCache;
    }

    public void deleteCacheData() {
        Path dir = Paths.get(CACHE_PATH);
        try {
            Files.walk(dir)
                    .sorted(Comparator.reverseOrder())
                    .forEach(path -> {
                        try {
                            if (path.compareTo(Path.of(CACHE_PATH)) != 0) {
                                Files.delete(path);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getAllGifs() {
        List<String> gifs = new ArrayList<String>();
        File[] dirs = new File(CACHE_PATH).listFiles();
        for (File dir : dirs) {
            if (dir.isDirectory()) {
                String path = CACHE_PATH + "/" + dir.getName();
                File[] files = new File(path).listFiles();
                for (File file : files) {
                    if (file.isFile()) {
                        gifs.add(file.getAbsolutePath().replaceAll("\\\\", "/"));
                    }
                }
            }
        }
        return gifs;
    }
}

